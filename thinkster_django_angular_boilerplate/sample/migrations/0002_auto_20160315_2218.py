# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sample', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='chapters',
        ),
        migrations.RemoveField(
            model_name='book',
            name='format',
        ),
        migrations.RemoveField(
            model_name='book',
            name='price',
        ),
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='book',
            name='title',
            field=models.TextField(),
        ),
    ]
