from django.db import models

class Book(models.Model):
    title = models.TextField()
    author = models.TextField()

    def __unicode__(self):
        return '{0}'.format(self.content)