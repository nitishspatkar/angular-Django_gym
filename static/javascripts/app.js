// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
            'thinkster',
            'ionic',
            'starter.controllers',
            'starter.services',
            'ngRoute'
      ])

.run(function($ionicPlatform, $rootScope, $state, Authentication) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
    //To secure routes when your is not authenticated only login and register states can be accessed
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      if (toState.authenticate && !Authentication.isAuthenticated()){
        // User isn’t authenticated
        $state.transitionTo("login");
        event.preventDefault();
      }
    });
})


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    //actual app related templates

    .state('login', {
      url: '/',
      templateUrl: '/static/templates/gym/login.html',
      controllerAs: 'vm',
      controller:'LoginController'
    })
    .state('register', {
      url: '/register',
      templateUrl: '/static/templates/gym/register.html',
      controllerAs: 'vm',
      controller:'RegisterController'
    })

  // setup an abstract state for the tabs directive
     .state('start', {
      url: '/start',
      templateUrl: '/static/templates/gym/start.html',
      controller:'startCtrl',
      authenticate: true
    })
    .state('tab', {
       url: '/tab',
       authenticate:true,
       abstract: true,
       templateUrl: '/static/templates/gym/tabs.html',
       controller: 'homeCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.intro', {
    url: '/intro',
    authenticate:true,
    views: {
      'tab-intro': {
        templateUrl: '/static/templates/gym/tab-introduction.html',
        controller: 'introCtrl'
        }
      }
  })

  .state('tab.plan', {
      url: '/plan',
      authenticate:true,
      views: {
        'tab-plan': {
          templateUrl: '/static/templates/gym/tab-plan.html',
          controller: 'planCtrl'
        }
      }
    })

    .state('tab.exercise', {
      url: '/plan/:dayId/:exerciseId',
      authenticate:true,
      views: {
        'tab-plan': {
          templateUrl: '/static/templates/gym/video.html',
          controller: 'videoCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    authenticate:true,
    views: {
      'tab-account': {
        templateUrl: '/static/templates/gym/tab-account.html',
        controller:'AccountController'
      }
    }
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

})

.factory('Plan', function() {
    // Might use a resource here that returns a JSON array

    // Some fake testing data
    var days = [
      { "id": 0,
        "name": 'Monday',
        "exercises":[
        {"id":0,"name":'Best Stretch'},
        {"id":1,"name":'Butterfly reverse'},
        {"id":2,"name":'SquatRow'},
        {"id":3,"name":'Plank'},
        {"id":4,"name":'Push Up'},
        {"id":5,"name":'Side Plank'},
        {"id":6,"name":'Squat'}
        ]
      },
      { "id": 1,
        "name": 'Tuesday',
        "exercises":[
          {"id":0,"name":'Best Stretch'},
          {"id":1,"name":'Butterfly reverse'},
          {"id":2,"name":'SquatRow'},
          {"id":3,"name":'Plank'},
          {"id":4,"name":'Push Up'},
          {"id":5,"name":'Side Plank'},
          {"id":6,"name":'Squat'}
        ]
      },
      { "id": 2,
        "name": 'Wednesday',
        "exercises":[
          {"id":0,"name":'Best Stretch'},
          {"id":1,"name":'Butterfly reverse'},
          {"id":2,"name":'SquatRow'},
          {"id":3,"name":'Plank'},
          {"id":4,"name":'Push Up'},
          {"id":5,"name":'Side Plank'},
          {"id":6,"name":'Squat'}
        ]
      },
      { "id": 3,
        "name": 'Thursday',
        "exercises":[
          {"id":0,"name":'Best Stretch'},
          {"id":1,"name":'Butterfly reverse'},
          {"id":2,"name":'SquatRow'},
          {"id":3,"name":'Plank'},
          {"id":4,"name":'Push Up'},
          {"id":5,"name":'Side Plank'},
          {"id":6,"name":'Squat'}
        ]
      },
      { "id": 4,
        "name": 'Friday',
        "exercises":[
          {"id":0,"name":'Best Stretch'},
          {"id":1,"name":'Butterfly reverse'},
          {"id":2,"name":'SquatRow'},
          {"id":3,"name":'Plank'},
          {"id":4,"name":'Push Up'},
          {"id":5,"name":'Side Plank'},
          {"id":6,"name":'Squat'}
        ]
      }
    ];

    return {
      all: function() {
        return days;
      },
      get: function(dayId) {
        // Simple index lookup
        return days[dayId];
      }
    }

  });
