angular.module('starter.controllers', ['ui.router'])

  .controller('AccountCtrl', function($scope, $http){

})

  .controller('homeCtrl', function($scope, $state){

    $scope.goHome = function(){
      $state.go('start');

    }

  })

  .controller('startCtrl', function($scope, $state, $http){

    $scope.startApp = function(){

      $state.go('tab.intro');
    };

    /** a sample api call to dummy app **/
    $http.get("/api/v1/sample/").then(function(response){

    $scope.response = response.data;

    });

  })

  .controller('introCtrl', function($scope, $state){

    $scope.startExercise = function(){

      $state.go('tab.plan');

    }
  })

  .controller('planCtrl', function($scope,$stateParams,Plan) {

      $scope.goHome = function(){
        $state.go('start');

      };

    $scope.days=Plan.all();


    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    }
  })

  .controller('videoCtrl', function($scope,$stateParams,Plan, $route, $location, $ionicPopup) {


   $scope.day = Plan.get($stateParams.dayId);
    $scope.exercise = $scope.day.exercises[$stateParams.exerciseId];

    var exerciseType = Number($stateParams.exerciseId);
    $scope.exerciseType = exerciseType;

    //Function to play next video
    $scope.playNext = function(){

      //Only increment till last exercise of the day
      if(exerciseType==6){
        exerciseType=-1;
      }

      //update url for next exercise
      $location.url('/tab/plan/'+ $stateParams.dayId + '/' + (exerciseType + 1));

    };

    //  Handle popup
     $scope.showBestStretchAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Best stretch',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };

    $scope.showButterflyReverseAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Butterfly Reverse',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };


    $scope.showSquatRowAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Squat Row',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };


    $scope.showPlankAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Plank',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };


    $scope.showPushUpAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Push Up',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };


    $scope.showSidePlankAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Side Plank',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };


    $scope.showSquatAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'More about Squat',
       template: 'Test'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };

  });

